<?php

declare(strict_types=1);

namespace ECommerce\Address\Repository;

use Paneric\ComponentModule\Interfaces\Repository\ModuleRepositoryInterface;

interface AddressRepositoryInterface extends ModuleRepositoryInterface
{}
