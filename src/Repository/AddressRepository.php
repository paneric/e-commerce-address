<?php

declare(strict_types=1);

namespace ECommerce\Address\Repository;

use Paneric\ComponentModule\Module\Repository\ModuleRepository;

class AddressRepository extends ModuleRepository implements AddressRepositoryInterface {}
