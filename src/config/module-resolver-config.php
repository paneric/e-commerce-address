<?php

declare(strict_types=1);

use Paneric\ModuleResolver\ModuleMapper;

$moduleMapper = new ModuleMapper();

$moduleMapCrossPaths = [
    'vendor/paneric/e-commerce-list-country',
    'vendor/paneric/e-commerce-list-type-company',

];

$moduleMapPath = 'vendor/paneric/e-commerce-address';

$moduleMap = [
    'adr'      => ROOT_FOLDER . '%s/src/AddressApp',
    'adrs'     => ROOT_FOLDER . '%s/src/AddressApp',
    'api-adr'  => ROOT_FOLDER . '%s/src/AddressApi',
    'api-adrs' => ROOT_FOLDER . '%s/src/AddressApi',
    'apc-adr'  => ROOT_FOLDER . '%s/src/AddressApc',
    'apc-adrs' => ROOT_FOLDER . '%s/src/AddressApc',
];

return [
    'default_route_key' => 'adr',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'merge_module_cross_map' => true,

    'module_map' => $moduleMapper->setModuleMap($moduleMap),

    'module_map_cross' => $moduleMapper->setModuleMapCross(
        $moduleMapCrossPaths,
        $moduleMap,
        $moduleMapPath,
        __DIR__ !== ROOT_FOLDER . 'src/config'
    ),
];
