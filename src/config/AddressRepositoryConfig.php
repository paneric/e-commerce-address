<?php

declare(strict_types=1);

namespace ECommerce\Address\config;

use ECommerce\Address\Gateway\AddressADAO;
use Paneric\Interfaces\Config\ConfigInterface;
use PDO;

class AddressRepositoryConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'table' => 'addresss',
            'dao_class' => AddressADAO::class,
            'fetch_mode' => PDO::FETCH_CLASS,//Data populated before dao constructor
            'create_unique_where' => sprintf(
                ' %s',
                'WHERE adr_ref=:adr_ref'
            ),
            'update_unique_where' => sprintf(
                ' %s %s',
                'WHERE adr_ref=:adr_ref',
                'AND adr_id NOT IN (:adr_id)'
            ),
            'select_query' => '
                SELECT *
                FROM addresss bt
                    INNER JOIN list_countrys st1 on bt.adr_list_country_id = st1.lc_id  
                    INNER JOIN list_type_companys st2 on bt.adr_list_type_company_id = st2.ltc_id
            ',
        ];
    }
}
