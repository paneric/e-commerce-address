<?php

declare(strict_types=1);

use Paneric\Middleware\RouteMiddleware;

if (isset($app, $container)) {
    $app->addMiddleware($container->get(RouteMiddleware::class));//always before UriMiddleware
    $app->addBodyParsingMiddleware();
    $app->addRoutingMiddleware();
    if (ENV === 'dev') {
        $errorMiddleware = $app->addErrorMiddleware(
            true,
            true,
            true,
            $loggerApp ?? null
        );
    } else {
        $errorMiddleware = $app->addErrorMiddleware(
            false,
            true,
            true,
            $loggerApp ?? null
        );
    }
}
