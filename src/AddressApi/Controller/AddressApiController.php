<?php

declare(strict_types=1);

namespace ECommerce\Address\AddressApi\Controller;

use Paneric\ComponentModule\Module\Controller\Relation\RelationModuleApiController;

class AddressApiController extends RelationModuleApiController {}
