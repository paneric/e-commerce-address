<?php

declare(strict_types=1);

use ECommerce\Address\config\AddressRepositoryConfig;
use ECommerce\Address\Repository\AddressRepository;
use ECommerce\Address\Repository\AddressRepositoryInterface;
use Paneric\DBAL\Manager;
use Psr\Container\ContainerInterface;

return [
    AddressRepositoryInterface::class => static function (ContainerInterface $container): AddressRepository {
        return new AddressRepository(
            $container->get(Manager::class),
            $container->get(AddressRepositoryConfig::class)
        );
    },
];
