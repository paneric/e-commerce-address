<?php

declare(strict_types=1);

namespace ECommerce\Address\AddressApi\config;

use ECommerce\Address\Gateway\AddressADAO;
use ECommerce\Address\Gateway\AddressADTO;
use Paneric\Interfaces\Config\ConfigInterface;

class AddressApiActionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'get_one_by_id' => [
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['adr_id' => (int) $id];
                },
            ],

            'get_all' => [
                'order_by' => static function (string $local = null): array
                {
                    return [];
                },
            ],

            'get_all_paginated' => [
                'find_by_criteria' => static function (string $local = null): array
                {
                    return [];
                },
                'order_by' => static function (string $local = null): array
                {
                    return [];
                },
            ],

            'create' => [
                'dao_class' => AddressADAO::class,
                'dto_class' => AddressADTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['adr_ref' => $attributes['ref']];
                },
            ],

            'create_multiple' => [
                'dao_class' => AddressADAO::class,
                'dto_class' => AddressADTO::class,
                'create_unique_criteria' => static function (array $collection): array
                {
                    $createUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $createUniqueCriteria[$index] = ['adr_ref' => $dao->getRef()];
                    }

                    return $createUniqueCriteria;
                },
            ],

            'update' => [
                'dao_class' => AddressADAO::class,
                'dto_class' => AddressADTO::class,
                'find_one_by_criteria' => static function (AddressADAO $dao, string $id): array
                {
                    return ['adr_id' => (int) $id, 'adr_ref' => $dao->getRef()];
                },
                'update_unique_criteria' => static function (string $id): array
                {
                    return ['adr_id' => (int) $id];
                },
            ],

            'update_multiple' => [
                'dao_class' => AddressADAO::class,
                'dto_class' => AddressADTO::class,
                'find_by_criteria' => static function (array $collection): array
                {
                    $findByCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $findByCriteria[$index] = [
                            'adr_id' => (int) $index,
                            'adr_ref' => $dao->getRef(),
                        ];
                    }

                    return $findByCriteria;
                },
                'update_unique_criteria' => static function (array $collection): array
                {
                    $updateUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $updateUniqueCriteria[$index] = [
                            'adr_id' => (int) $index,
                        ];
                    }

                    return $updateUniqueCriteria;
                },
            ],

            'delete' => [
                'delete_by_criteria' => static function (array $attributes): array
                {
                    $deleteByCriteria = [];

                    foreach ($attributes as $key => $value) {
                        $deleteByCriteria['adr_' . $key] = (int) $value;
                    }

                    return $deleteByCriteria;
                },
            ],

            'delete_multiple' => [
                'dao_class' => AddressADAO::class,
                'dto_class' => AddressADTO::class,
                'delete_by_criteria' => static function (array $collection): array
                {
                    $deleteByCriteria = [];

                    foreach ($collection as $index => $dao) {
                            $deleteByCriteria[$index]['adr_id'] = (int) $dao->getId();
                    }

                    return $deleteByCriteria;
                },
            ],
        ];
    }
}
