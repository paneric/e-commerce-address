<?php

declare(strict_types=1);

namespace ECommerce\Address\Gateway;

use ECommerce\ListCountry\Gateway\ListCountryDAO;
use ECommerce\ListTypeCompany\Gateway\ListTypeCompanyDAO;
use Paneric\DataObject\ADAO;

class AddressADAO extends ADAO
{
    protected $id;
    protected $listCountryId;
    protected $listTypeCompanyId;

    protected $listCountry;
    protected $listTypeCompany;

    protected $ref;//string
    protected $surname;//String
    protected $name;//String

    public function __construct()
    {
        $this->prefix = 'adr_';

        $this->setMaps();

        if ($this->values) {
            $this->listCountry = new ListCountryDAO();
            $this->values = $this->listCountry->hydrate($this->values);

            $this->listTypeCompany = new ListTypeCompanyDAO();
            $this->values = $this->listTypeCompany->hydrate($this->values);

            $this->hydrate($this->values);

            unset($this->values);
        }
    }

    /**
     * @return null|int|string
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return null|int|string
     */
    public function getListCountryId()
    {
        return $this->listCountryId;
    }
    /**
     * @return null|int|string
     */
    public function getListTypeCompanyId()
    {
        return $this->listTypeCompanyId;
    }


    public function getListCountry(): ?ListCountryDAO
    {
        return $this->listCountry;
    }
    public function getListTypeCompany(): ?ListTypeCompanyDAO
    {
        return $this->listTypeCompany;
    }


    public function getRef(): ?String
    {
        return $this->ref;
    }
    public function getSurname(): ?String
    {
        return $this->surname;
    }
    public function getName(): ?String
    {
        return $this->name;
    }

    /**
     * @var int|string
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @var int|string
     */
    public function setListCountryId($listCountryId): void
    {
        $this->listCountryId = $listCountryId;
    }
    /**
     * @var int|string
     */
    public function setListTypeCompanyId($listTypeCompanyId): void
    {
        $this->listTypeCompanyId = $listTypeCompanyId;
    }


    public function setListCountry(ListCountryDAO $listCountry): void
    {
        $this->listCountry = $listCountry;
    }
    public function setListTypeCompany(ListTypeCompanyDAO $listTypeCompany): void
    {
        $this->listTypeCompany = $listTypeCompany;
    }


    public function setRef(String $ref): void
    {
        $this->ref = $ref;
    }
    public function setSurname(String $surname): void
    {
        $this->surname = $surname;
    }
    public function setName(String $name): void
    {
        $this->name = $name;
    }

}
