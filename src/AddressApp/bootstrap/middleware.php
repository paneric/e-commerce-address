<?php

declare(strict_types=1);

use Paneric\Middleware\RouteMiddleware;
use Paneric\Middleware\UriMiddleware;
use Paneric\Session\SessionMiddleware;

if (isset($app, $container)) {
    $app->addMiddleware($container->get(UriMiddleware::class));
    $app->addMiddleware($container->get(RouteMiddleware::class));//always before UriMiddleware
    $app->addMiddleware($container->get(SessionMiddleware::class));
    $app->addBodyParsingMiddleware();
    $app->addRoutingMiddleware();
    if (ENV === 'dev') {
        $errorMiddleware = $app->addErrorMiddleware(
            true,
            true,
            true,
            $loggerApp ?? null
        );
    } else {
        $errorMiddleware = $app->addErrorMiddleware(
            false,
            true,
            true,
            $loggerApp ?? null
        );
    }
}
