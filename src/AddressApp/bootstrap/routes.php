<?php

declare(strict_types=1);

use ECommerce\Address\AddressApp\Controller\AddressAppController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Pagination\PaginationMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {

    $app->map(['GET'], '/adr/show-one-by-id/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(AddressAppController::class)->showOneById(
            $response,
            $this->get('address_get_one_by_id_app_action'),
            $args['id'] ?? '1'
        );
    })->setName('adr.show-one-by-id');


    $app->map(['GET'], '/adrs/show-all', function(Request $request, Response $response) {
        return $this->get(AddressAppController::class)->showAll(
            $response,
            $this->get('address_get_all_app_action')
        );
    })->setName('adrs.show-all');

    $app->map(['GET'], '/adrs/show-all-paginated[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(AddressAppController::class)->showAllPaginated(
            $request,
            $response,
            $this->get('address_get_all_paginated_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('adrs.show-all-paginated')
        ->addMiddleware($container->get(PaginationMiddleware::class));


    $app->map(['GET', 'POST'], '/adr/add', function(Request $request, Response $response) {
        return $this->get(AddressAppController::class)->add(
            $request,
            $response,
            $this->get('address_app_sub_data'),
            $this->get('address_create_app_action')
        );
    })->setName('adr.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/adrs/add', function(Request $request, Response $response) {
        return $this->get(AddressAppController::class)->addMultiple(
            $request,
            $response,
            $this->get('address_app_sub_data'),
            $this->get('address_create_multiple_app_action')
        );
    })->setName('adrs.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));


    $app->map(['GET', 'POST'], '/adr/edit/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(AddressAppController::class)->edit(
            $request,
            $response,
            $this->get('address_app_sub_data'),
            $this->get('address_get_one_by_id_app_action'),
            $this->get('address_update_app_action'),
            $args['id']
        );
    })->setName('adr.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/adrs/edit[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(AddressAppController::class)->editMultiple(
            $request,
            $response,
            $this->get('address_app_sub_data'),
            $this->get('address_get_all_paginated_app_action'),
            $this->get('address_update_multiple_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('adrs.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(PaginationMiddleware::class));


    $app->map(['GET', 'POST'], '/adr/remove/{id}', function(Request $request, Response $response, array $args) {
        return $this->get(AddressAppController::class)->remove(
            $request,
            $response,
            $this->get('address_delete_app_action'),
            $args['id']
        );
    })->setName('adr.remove')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/adrs/remove[/{page}]', function(Request $request, Response $response, array $args) {
        return $this->get(AddressAppController::class)->removeMultiple(
            $request,
            $response,
            $this->get('address_get_all_paginated_app_action'),
            $this->get('address_delete_multiple_app_action'),
            $args['page'] ?? '1'
        );
    })->setName('adrs.remove')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(PaginationMiddleware::class));
}
