<?php

declare(strict_types=1);

namespace ECommerce\Address\AddressApp\config;

use ECommerce\Address\Gateway\AddressADAO;
use ECommerce\Address\Gateway\AddressADTO;
use Paneric\Interfaces\Config\ConfigInterface;

class AddressAppActionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'get_one_by_id' => [
                'module_name_sc' => 'address',
                'prefix' => 'adr',
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['adr_id' => (int) $id];
                },
            ],

            'get_all' => [
                'module_name_sc' => 'address',
                'prefix' => 'adr',
                'order_by' => static function (string $local = null): array
                {
                    return [];
                },
            ],

            'get_all_paginated' => [
                'module_name_sc' => 'address',
                'prefix' => 'adr',
                'find_by_criteria' => static function (string $local = null): array
                {
                    return [];
                },
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ],

            'create' => [
                'module_name_sc' => 'address',
                'prefix' => 'adr',
                'dao_class' => AddressADAO::class,
                'dto_class' => AddressADTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['adr_ref' => $attributes['ref']];
                },
            ],

            'create_multiple' => [
                'module_name_sc' => 'address',
                'prefix' => 'adr',
                'dao_class' => AddressADAO::class,
                'dto_class' => AddressADTO::class,
                'create_unique_criteria' => static function (array $collection): array
                {
                    $createUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $createUniqueCriteria[$index] = ['adr_ref' => $dao->getRef()];
                    }

                    return $createUniqueCriteria;
                },
            ],

            'update' => [
                'module_name_sc' => 'address',
                'prefix' => 'adr',
                'dao_class' => AddressADAO::class,
                'dto_class' => AddressADTO::class,
                'find_one_by_criteria' => static function (AddressADAO $dao, string $id): array
                {
                    return ['adr_id' => (int) $id, 'adr_ref' => $dao->getRef()];
                },
                'update_unique_criteria' => static function (string $id): array
                {
                    return ['adr_id' => (int) $id];
                },
            ],

            'update_multiple' => [
                'module_name_sc' => 'address',
                'prefix' => 'adr',
                'dao_class' => AddressADAO::class,
                'dto_class' => AddressADTO::class,
                'find_by_criteria' => static function (array $collection): array
                {
                    $findByCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $findByCriteria[$index] = [
                            'adr_id' => (int) $index,
                            'adr_ref' => $dao->getRef(),
                        ];
                    }

                    return $findByCriteria;
                },

                'update_unique_criteria' => static function (array $collection): array
                {
                    $updateUniqueCriteria = [];

                    foreach ($collection as $index => $dao) {
                        $updateUniqueCriteria[$index] = [
                            'adr_id' => (int) $index,
                        ];
                    }

                    return $updateUniqueCriteria;
                },
            ],

            'delete' => [
                'module_name_sc' => 'address',
                'delete_by_criteria' => static function (array $attributes): array
                {
                    $deleteByCriteria = [];

                    foreach ($attributes as $key => $value) {
                        $deleteByCriteria['adr_' . $key] = (int) $value;
                    }

                    return $deleteByCriteria;
                },
            ],

            'delete_multiple' => [
                'module_name_sc' => 'address',
                'dao_class' => AddressADAO::class,
                'dto_class' => AddressADTO::class,
                'delete_by_criteria' => static function (array $daoCollection): array
                {
                    $deleteByCriteria = [];

                    foreach ($daoCollection as $index => $dao) {
                            $deleteByCriteria[$index]['adr_id'] = (int) $dao->getId();

                    }

                    return $deleteByCriteria;
                },
            ],
        ];
    }
}
