<?php

use ECommerce\Address\AddressApp\config\AddressAppActionConfig;
use ECommerce\Address\AddressApp\config\AddressAppControllerConfig;
use ECommerce\Address\AddressApp\Controller\AddressAppController;
use ECommerce\Address\Repository\AddressRepositoryInterface;
use Paneric\ComponentModule\Module\Action\App\CreateAppAction;
use Paneric\ComponentModule\Module\Action\App\CreateMultipleAppAction;
use Paneric\ComponentModule\Module\Action\App\DeleteAppAction;
use Paneric\ComponentModule\Module\Action\App\DeleteMultipleAppAction;
use Paneric\ComponentModule\Module\Action\App\GetAllAppAction;
use Paneric\ComponentModule\Module\Action\App\GetAllPaginatedAppAction;
use Paneric\ComponentModule\Module\Action\App\GetOneByIdAppAction;
use Paneric\ComponentModule\Module\Action\App\UpdateAppAction;
use Paneric\ComponentModule\Module\Action\App\UpdateMultipleAppAction;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Container\ContainerInterface;
use Twig\Environment as Twig;

return [

    AddressAppController::class => static function(ContainerInterface $container): AddressAppController
    {
        return new AddressAppController(
            $container->get(Twig::class),
            $container->get(AddressAppControllerConfig::class),
        );
    },

    'address_create_app_action' => static function (ContainerInterface $container): CreateAppAction
    {
        return new CreateAppAction (
            $container->get(AddressRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(AddressAppActionConfig::class),
        );
    },

    'address_create_multiple_app_action' => static function (ContainerInterface $container): CreateMultipleAppAction
    {
        return new CreateMultipleAppAction (
            $container->get(AddressRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(AddressAppActionConfig::class),
        );
    },

    'address_delete_app_action' => static function (ContainerInterface $container): DeleteAppAction
    {
        return new DeleteAppAction (
            $container->get(AddressRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(AddressAppActionConfig::class),
        );
    },

    'address_delete_multiple_app_action' => static function (ContainerInterface $container): DeleteMultipleAppAction
    {
        return new DeleteMultipleAppAction (
            $container->get(AddressRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(AddressAppActionConfig::class),
        );
    },

    'address_get_all_app_action' => static function (ContainerInterface $container): GetAllAppAction
    {
        return new GetAllAppAction (
            $container->get(AddressRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(AddressAppActionConfig::class),
        );
    },

    'address_get_all_paginated_app_action' => static function (ContainerInterface $container): GetAllPaginatedAppAction
    {
        return new GetAllPaginatedAppAction (
            $container->get(AddressRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(AddressAppActionConfig::class),
        );
    },

    'address_get_one_by_id_app_action' => static function (ContainerInterface $container): GetOneByIdAppAction
    {
        return new GetOneByIdAppAction (
            $container->get(AddressRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(AddressAppActionConfig::class),
        );
    },

    'address_update_app_action' => static function (ContainerInterface $container): UpdateAppAction
    {
        return new UpdateAppAction (
            $container->get(AddressRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(AddressAppActionConfig::class),
        );
    },

    'address_update_multiple_app_action' => static function (ContainerInterface $container): UpdateMultipleAppAction
    {
        return new UpdateMultipleAppAction (
            $container->get(AddressRepositoryInterface::class),
            $container->get(SessionInterface::class),
            $container->get(AddressAppActionConfig::class),
        );
    },
];
