<?php

declare(strict_types=1);

use ECommerce\Address\Gateway\AddressADTO;

return [

    'validation' => [

        'adr.add' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ],
                    'surname' => [
                        'required' => [],
                    ],
                    'name' => [
                        'required' => [],
                    ],
                    'list_country_id' => [
                        'required' => [],
                    ],
                    'list_type_company_id' => [
                        'required' => [],
                    ],

                ],
            ],
        ],

        'adrs.add' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ],
                    'surname' => [
                        'required' => [],
                    ],
                    'name' => [
                        'required' => [],
                    ],
                    'list_country_id' => [
                        'required' => [],
                    ],
                    'list_type_company_id' => [
                        'required' => [],
                    ],

                ],
            ],
        ],

        'adr.edit' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ],
                    'surname' => [
                        'required' => [],
                    ],
                    'name' => [
                        'required' => [],
                    ],
                    'list_country_id' => [
                        'required' => [],
                    ],
                    'list_type_company_id' => [
                        'required' => [],
                    ],

                ],
            ],
        ],

        'adrs.edit' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                    ],
                    'surname' => [
                        'required' => [],
                    ],
                    'name' => [
                        'required' => [],
                    ],
                    'list_country_id' => [
                        'required' => [],
                    ],
                    'list_type_company_id' => [
                        'required' => [],
                    ],

                ],
            ],
        ],

        'adr.remove' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],

        'adrs.remove' => [
            'methods' => ['POST'],
            AddressADTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                    ],
                ],
            ],
        ],
    ]
];
