<?php

declare(strict_types=1);

namespace ECommerce\Address\AddressApp\Controller;

use Paneric\ComponentModule\Module\Controller\Relation\RelationModuleAppController;

class AddressAppController extends RelationModuleAppController {}
