<?php

declare(strict_types=1);

namespace ECommerce\Address\AddressApc\Controller;

use Paneric\ComponentModule\Module\Controller\Relation\RelationModuleApcController;

class AddressApcController extends RelationModuleApcController {}
