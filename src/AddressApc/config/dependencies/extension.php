<?php

declare(strict_types=1);

use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Pagination\PaginationExtension;
use Paneric\Twig\Extension\CSRFExtension;
use Paneric\Twig\Extension\ValidationExtension;
use Paneric\Validation\Validator;
use Paneric\Validation\ValidatorBuilder;
use Psr\Container\ContainerInterface;

return [
    CSRFExtension::class => static function (ContainerInterface $container): CSRFExtension
    {
        return new CSRFExtension(
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $container->get('csrf')
        );
    },

    Validator::class => static function (ContainerInterface $container): Validator {
        $validatorBuilder = new ValidatorBuilder();

        return $validatorBuilder->build(
            (string) $container->get('local')
        );
    },

    ValidationExtension::class => static function (ContainerInterface $container): ValidationExtension
    {
        return new ValidationExtension(
            $container->get(Validator::class)
        );
    },

    PaginationExtension::class => static function (ContainerInterface $container): PaginationExtension
    {
        return new PaginationExtension(
            $container->get(SessionInterface::class),
            $container->get('pagination-extension')
        );
    },
];
