<?php

declare(strict_types=1);

namespace ECommerce\Address\AddressApc\config;

use Paneric\Interfaces\Config\ConfigInterface;
use ECommerce\Address\Gateway\AddressADAO;

class AddressApcActionConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        $apiEndpoints = [
            'base_url' => $_ENV['BASE_API_URL'],

            'api-prefix.get'       => '/api-adr/get/',
            'api-prefixs.get'      => '/api-adrs/get',
            'api-prefixs.get.page' => '/api-adrs/get/',

            'api-prefix.create'    => '/api-adr/create',
            'api-prefixs.create'   => '/api-adrs/create',

            'api-prefix.update'    => '/api-adr/update/',
            'api-prefixs.update'   => '/api-adrs/update',

            'api-prefix.delete'    => '/api-adr/delete/',
            'api-prefixs.delete'   => '/api-adrs/delete',

            'apc-prefixs.show-all-paginated' => '/apc-adrs/show-all-paginated/',
            'apc-prefixs.edit'               => '/apc-adrs/edit/',
            'apc-prefixs.remove'             => '/apc-adrs/remove/',
        ];

        return [
            'get_one_by_id' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'address',
                    'prefix' => 'adr'
                ]
            ),

            'get_all' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'address',
                    'prefix' => 'adr'
                ]
            ),

            'get_all_paginated' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'address',
                    'prefix' => 'adr'
                ]
            ),


            'create' => array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'address',
                    'prefix' => 'adr'
                ]
            ),

            'create_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'address',
                    'dao_class' => AddressADAO::class,
                    'prefix' => 'adr'
                ]
            ),

            'update' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'address',
                    'prefix' => 'adr'
                ]
            ),

            'update_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'address',
                ]
            ),

            'delete' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'address',
                ]
            ),

            'delete_multiple' =>  array_merge(
                $apiEndpoints,
                [
                    'module_name_sc' => 'address',
                ]
            ),
        ];
    }
}
