<?php

declare(strict_types=1);

namespace ECommerce\Address\AddressApc\config;

use Paneric\Interfaces\Config\ConfigInterface;

class AddressApcControllerConfig implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'route_prefix' => 'adr'
        ];
    }
}
