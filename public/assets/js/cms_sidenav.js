document.addEventListener('click',function (e) {
    if (e.target.id === 'menu_icon_open') {
        openNav();
    }
    if (e.target.id === 'menu_icon_close') {
        closeNav();
    }
});

let openNav = function () {
    document.getElementById('cms_sidenav').style.width = "270px";
}

/* Set the width of the side navigation to 0 */
let closeNav = function () {
    document.getElementById('cms_sidenav').style.width = "0";
}
