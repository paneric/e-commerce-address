# RELATION MICRO SERVICE

## Clone repository

```sh
$ git clone https://paneric@bitbucket.org/paneric/relation-micro-service.git
$ mv relation-micro-service e-commerce-address
$ cd e-commerce-address
$ rm -r -f .git
```

## Set a custom module

Adapt project `name` and `psr-4`:

*composer.json*
```json
{
    "name": "paneric/e-commerce-address",
    ...
    "autoload": {
        "psr-4": {
            "ECommerce\\Address\\": "src"
        }
    },
    ...
}
```

Run composer:

```sh
$ composer update
$ composer require paneric/e-commerce-list-country
$ composer require paneric/e-commerce-list-type-company
```

Make sure the `scope` variable is set as `lib`:

*bin/app*
```php
#!/usr/bin/env php
<?php

$scope = 'lib';
...
```
Run console command:

```sh
$ bin/app rm

Psr-4 (no trailing slash !!!): ECommerce
Vendor service folder path (relative to project folder, no trailing slash !!!): vendor/paneric/e-commerce-address                                                 
Service name (CamelCase !!!): Address
Service prefix (lower case !!!): adr
Vendor sub services folders paths (coma separated, relative to project folder, no trailing slash !!!): vendor/paneric/e-commerce-list-country,vendor/paneric/e-commerce-list-type-company         
Sub services names (CamelCase, coma separated !!!): ListCountry,ListTypeCompany
Sub services prefixes (lower case, coma separated !!!): lc,ltc
DAO attributes names (camelCase, coma separated !!!): ref,surname,name
DAO attributes types (CamelCase, coma separated !!!): String,String,String
DAO unique attribute name (camelCase !!!): ref

                                                                          
  RELATION MICRO SERVICE UPDATE SUCCESS:                                  
                                                                          

  Resources update with vendor "ECommerce", service "Address" and prefix "adr" success.
```
## Manual update

### Templates:

> If necessary update `local` with other chosen label.

*src/AddressApc/templates/add.html.twig*  
*src/AddressApc/templates/edit.html.twig*  
*src/AddressApp/templates/add.html.twig*  
*src/AddressApp/templates/edit.html.twig*  
```html
<div class="form-group select {{ vld_style('list_country_id') }}">
    <select class="form-group-item form-group-select" name="list_country_id" autocomplete="off" required>
        {% set lc_init = lcs[19] %}
        <option value="{{ lc_init.id }}">{{ attribute(lc_init, local)|trans }}</option>
        {% for lc in lcs %}
        {% if adr.list_country_id == lc.id %}
        <option value="{{ lc.id }}" selected>{{ attribute(lc, local) }}</option>
        {% else %}
        <option value="{{ lc.id }}">{{ attribute(lc, local) }}</option>
        {% endif %}
        {% endfor %}
    </select>
    <label for="name" class="form-group-item form-group-label-select"><span>{{ ('list_country')|trans }}</span></label>
</div>
{{ vld_feedback('list_country_id')|raw }}
```

### DAO:

> In case of generice Int attributes it is necessary to adapt getter and setter:

*src/Gateway/AddressDAO.php*

```php
    /**
     * @return null|int|string
     */
    public function getIsActive()
    {
        return $this->isActive;
    } 

    /**
     * @var int|string
     */
    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    } 
```

### Validation:

*src/AddressApi/config/settings/validation.php*  
*src/AddressApp/config/settings/validation.php*
```php
                'rules' => [
                    'list_country_id' => [
                        'required' => [],
                    ], 
                    'list_type_company_id' => [
                        'required' => [],                    
                    ], 

                    'ref' => [
                        'required' => [],                    
                    ], 
                    'surname' => [
                        'required' => [],                    
                    ], 
                    'name' => [
                        'required' => [],                    
                    ], 

                ],
```


## Environment variables
*.env*
```
ENV = 'dev'
APP_DOMAIN = '127.0.0.1:8080'
BASE_API_URL = 'http://127.0.0.1:8081'

# GUARD:
KEY_ASCII = 'def000008d147968b763eac2453624272c423c77347bd423e736328d648365fb243ea2130953f36ff38b5c9d8b15e5063df9e014010a3e06a1ec60612e54737a9f54f45d'
# DB:
DB_HOST = 'localhost'
DB_NAME = 'e_commerce'
DB_USR = 'toor'
DB_PSWD = 'toor'
JWT_SECRET = 'NRA4kcEMGUr+w5zOWpsVJ7v4NmQVVAuMZfZFll5g4pjnTGHJFj8Fw24bvUlRsttJrY0/ZBtav66nCzuD0S/rrBuNRGsV8QkulREVa9krRxTO/mnx1fgVIPBMacHMm6hN'
```

## Migrations and fixtures

#### Install migrations
```sh
$ composer require --dev robmorgan/phinx
```
#### Create migrations and seed folders
```sh
$ mkdir db
$ cd db
$ mkdir migrations
$ mkdir seeds
```
#### Init migrations settings
```sh
$ bin/phinx init
```
> That will create `phinx.php` file in your root directory. Update your db credentials.

*phinx.php*
```php
return
[
    ...
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_environment' => 'development',
        'production' => [
            'adapter' => 'mysql',
            'host' => 'localhost',
            'name' => 'e_commerce',
            'user' => 'toor',
            'pass' => 'toor',
            'port' => '3306',
            'charset' => 'utf8',
        ],
        'development' => [
            ...
        ],
        'testing' => [
            ...
        ]
    ],
    'version_order' => 'creation'
];
```

#### [Create your migration](https://book.cakephp.org/phinx/0/en/migrations.html)
```sh
$ bin/phinx create SessionsMigration
```

*db/migrations/20210208151830_address_migration.php*
```php
use Phinx\Migration\AbstractMigration;

final class AddressMigration extends AbstractMigration
{
    public function change(): void
    {
        ...
    }
}
```

#### [Create your fixtures](https://book.cakephp.org/phinx/0/en/migrations.html)
```sh
$ composer require --dev fzaninotto/faker
```
```sh
$ bin/phinx seed:create SessionsSeeder
```

*db/seeds/SessionsSeeder.php*
```php
use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class AddressSeeder extends AbstractSeed
{
    public function run()
    {
        ...
    }
}
```

#### Run migrations (standard)
```sh
$ bin/phinx migrate -e development
```
#### Run migrations (Docker)
```sh
$ sudo docker exec 57f bin/phinx migrate -e development  
```

#### Run fixtures (standard)
```sh
$ bin/phinx seed:run -e development
```
#### Run fixtures (Docker)
```sh
$ sudo docker exec 57f bin/phinx seed:run -e development
```
#### [Migrations and fixtures commands](https://book.cakephp.org/phinx/0/en/commands.html)


## Apache

Create log folder:
```sh
$ mkdir var
$ mkdir var/cache
$ mkdir var/logs
$ sudo chmod -R 777 var
```

*/etc/apache2/sites-available*
```sh
$ cp 000-default.conf e-commerce.conf
```

*/etc/apache2/sites-available/e-commerce.conf*
```
Listen 8080
Listen 8081

<VirtualHost 127.0.0.1:8080>
    ServerAdmin webmaster@e-commerce.dev
    ServerName e-commerce-address.dev
    ServerAlias www.e-commerce-address.dev

    DocumentRoot /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-address
    
    <Directory /home/paneric/Documents/php-dev/E-COMMERCE/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-address/var/apache_error.log
    CustomLog /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-address/var/access.log combined
</VirtualHost>

<VirtualHost 127.0.0.1:8081>
    ServerAdmin webmaster@e-commerce.dev
    ServerName e-commerce-address.dev
    ServerAlias www.e-commerce-address.dev

    DocumentRoot /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-address
    
    <Directory /home/paneric/Documents/php-dev/E-COMMERCE/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-address/var/apache_error.log
    CustomLog /home/paneric/Documents/php-dev/E-COMMERCE/e-commerce-address/var/access.log combined
</VirtualHost>
```

*/etc/apache2/sites-available*
```
$ sudo a2ensite e-commerce.conf  
$ sudo a2dissite 000-default.conf
```


Ensure that the Apache mod_rewrite module is installed and enabled. In order to enable mod_rewrite you can type the following command in the terminal:
```sh
$ sudo a2enmod rewrite
$ sudo a2enmod actions
```

Restart Apache:
```sh
$ sudo systemctl restart apache2  
$ systemctl status apache2.service
$ sudo apachectl configtest
```

*e-commerce-address/.htaccess*
```
RewriteEngine On
RewriteCond %{REQUEST_URI} !^/public
RewriteRule ^(.*)$ /public/$1 [NC,L]

RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
```

*e-commerce-address/public/.htaccess*
```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^ index.php [QSA,L]

RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
```
